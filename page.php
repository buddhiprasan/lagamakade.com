<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package lagamakade
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
get_header();
?>

<div class="wrapper" id="page-wrapper">
    <div class="container-fluid">
        <div class="row justify-content-center">

            <?php while ( have_posts() ) : the_post(); ?>
            <?php get_template_part( 'loop-templates/content', 'page' ); ?>
            <?php
				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;
				?>
            <?php endwhile; // end of the loop. ?>

        </div>
    </div>
</div><!-- #page-wrapper -->

<?php get_footer();