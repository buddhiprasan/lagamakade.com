<?php
/**
 * Content empty partial template
 *
 * @package lagamakade
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

the_content();
