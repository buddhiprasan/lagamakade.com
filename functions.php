<?php
/**
 * lagamakade functions and definitions
 *
 * @package lagamakade
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

$lagamakade_includes = array(
	'/theme-settings.php',                  // Initialize theme default settings.
	'/setup.php',                           // Theme setup and custom theme supports.
	'/widgets.php',                         // Register widget area.
	'/enqueue.php',                         // Enqueue scripts and styles.
	'/template-tags.php',                   // Custom template tags for this theme.
	'/pagination.php',                      // Custom pagination for this theme.
	'/hooks.php',                           // Custom hooks.
	'/extras.php',                          // Custom functions that act independently of the theme templates.
	'/customizer.php',                      // Customizer additions.
	'/custom-comments.php',                 // Custom Comments file.
	'/jetpack.php',                         // Load Jetpack compatibility file.
	'/class-wp-bootstrap-navwalker.php',    // Load custom WordPress nav walker. Trying to get deeper navigation? Check out: https://github.com/lagamakade/lagamakade/issues/567
	'/woocommerce.php',                     // Load WooCommerce functions.
	'/editor.php',                          // Load Editor functions.
	'/deprecated.php',                      // Load deprecated functions.
);

foreach ( $lagamakade_includes as $file ) {
	require_once get_template_directory() . '/inc' . $file;
}



add_action( 'woocommerce_after_shop_loop_item_title', 'wc_add_short_description' );
/**
 * WooCommerce, Add Short Description to Products on Shop Page
 *
 * @link https://wpbeaches.com/woocommerce-add-short-or-long-description-to-products-on-shop-page
 */
function wc_add_short_description() {
	global $product;

	?>
<div itemprop="description" class="product_cart_price">
    <?php echo apply_filters( 'woocommerce_short_description', $product->post-> post_excerpt ) ?>
</div>
<?php
}

//Hide Price Range for WooCommerce Variable Products
add_filter( 'woocommerce_variable_sale_price_html', 
'lw_variable_product_price', 10, 2 );
add_filter( 'woocommerce_variable_price_html', 
'lw_variable_product_price', 10, 2 );

function lw_variable_product_price( $v_price, $v_product ) {
// Product Price
$prod_prices = array( $v_product->get_variation_price( 'min', true ), 
                            $v_product->get_variation_price( 'max', true ) );
$prod_price = $prod_prices[0]!==$prod_prices[1] ? sprintf(__(' %1$s', 'woocommerce'), 
                       wc_price( $prod_prices[0] ) ) : wc_price( $prod_prices[0] );

// Regular Price
$regular_prices = array( $v_product->get_variation_regular_price( 'min', true ), 
                          $v_product->get_variation_regular_price( 'max', true ) );
sort( $regular_prices );
$regular_price = $regular_prices[0]!==$regular_prices[1] ? sprintf(__(' %1$s','woocommerce')
                      , wc_price( $regular_prices[0] ) ) : wc_price( $regular_prices[0] );

if ( $prod_price !== $regular_price ) {
$prod_price = '<del>'.$regular_price.$v_product->get_price_suffix() . '</del> <ins>' . 
                       $prod_price . $v_product->get_price_suffix() . '</ins>';
}
return $prod_price;
}


/**
* @snippet Turn Address Checkout Field Into Drop-down  - WooCommerce
* @how-to Get CustomizeWoo.com FREE
* @sourcecode https://businessbloomer.com/?p=73350
* @author Rodolfo Melogli
* @testedwith WooCommerce 3.2.5
*/
 
add_filter( 'woocommerce_default_address_fields' , 'bbloomer_address_field_dropdown' );
 
function bbloomer_address_field_dropdown( $address_fields ) {
 
    $location_array = array(
     

        'Colombo 01' => 'Colombo 01',
        'Colombo 02' => 'Colombo 02',
        'Colombo 03' => 'Colombo 03',
        'Colombo 04' => 'Colombo 04',
        'Colombo 05' => 'Colombo 05',
        'Colombo 06' => 'Colombo 06',
        'Colombo 07' => 'Colombo 07',
        'Colombo 08' => 'Colombo 08',
        'Colombo 09' => 'Colombo 09',
        'Colombo 10' => 'Colombo 10',
        'Colombo 11' => 'Colombo 11',
        'Colombo 12' => 'Colombo 12',
        'Colombo 13' => 'Colombo 13',
        'Colombo 14' => 'Colombo 14',
        'Colombo 15' => 'Colombo 15',
        'Battaramulla' => 'Battaramulla',
        'Dehiwala' => 'Dehiwala',
        'Kolonnawa' => 'Kolonnawa',
        'Maharagama' => 'Maharagama',
        'Mount Lavinia' => 'Mount Lavinia',
        'Mulleriyawa New Town' => 'Mulleriyawa New Town',
        'Mutwal' => 'Mutwal',
        'Nugegoda' => 'Nugegoda',
        'Sri Jayawardenepura' => 'Sri Jayawardenepura',
        'Talawatugoda' => 'Talawatugoda',
        'Wattala' => 'Wattala',
        'Attidiya' => 'Attidiya',
        'Delkanda' => 'Delkanda',
        'Gangodawila' => 'Gangodawila',
        'Kalubowila' => 'Kalubowila',
        'Kohuwala' => 'Kohuwala',
        'Kotikawatta' => 'Kotikawatta',
        'Malabe' => 'Malabe',
        'Narahempita' => 'Narahempita',
        'Nawala' => 'Nawala',
        'Polhengoda' => 'Polhengoda',
        'Rajagiriya' => 'Rajagiriya',
        'Ratmalana' => 'Ratmalana',
        'Athurugiriya' => 'Athurugiriya',
        'Batawala' => 'Batawala',
        'Bope' => 'Bope',
        'Dedigamuwa' => 'Dedigamuwa',
        'Deltara (Jalthara)' => 'Deltara (Jalthara)',
        'Habarakada' => 'Habarakada',
        'Hiripitya (kottawa)' => 'Hiripitya (kottawa)',
        'Hokandara' => 'Hokandara',
        'Homagama' => 'Homagama',
        'Horagala' => 'Horagala',
        'Kaduwela' => 'Kaduwela',
        'Kiriwattuduwa' => 'Kiriwattuduwa',
        'Madapatha' => 'Madapatha',
        'Malabe' => 'Malabe',
        'Meegoda' => 'Meegoda',
        'Moratuwa' => 'Moratuwa',
        'Mullegama' => 'Mullegama',
        'Mullegama' => 'Mullegama',
        'Padukka' => 'Padukka',
        'Pannipitiya' => 'Pannipitiya',
        'Pitipana Homagama' => 'Pitipana Homagama',
        'Polgasowita' => 'Polgasowita',
        'Ranala' => 'Ranala',
        'Siddamulla' => 'Siddamulla',
        'Watareka' => 'Watareka',
        
'Kotuwegoda' => 'Kotuwegoda',

'Uyanwatta' => 'Uyanwatta',

'Matara Fort' => 'Matara Fort',

'Meddawatta' => 'Meddawatta',

'Weraduwa' => 'Weraduwa',

'Weherahena' => 'Weherahena',

'Kekanadura' => 'Kekanadura',

'Walpala' => 'Walpala',

'Weragampita' => 'Weragampita',

'Gabadaweediya' => 'Gabadaweediya',

'Tudawa' => 'Tudawa',

'Nadugala' => 'Nadugala',

'Bandattara' => 'Bandattara',

'Watagedara' => 'Watagedara',

'Palatuwa' => 'Palatuwa',

'Godagama' => 'Godagama',

'Isadeen Town' => 'Isadeen Town',

'Hittatiya' => 'Hittatiya',

'Nupe' => 'Nupe',

'Totamuna' => 'Totamuna',

'Pamburana' => 'Pamburana',

'Welegoda' => 'Welegoda',

'Walgama' => 'Walgama',

'Dewinuwara' => 'Dewinuwara',

'Eliyakanda' => 'Eliyakanda',

'Pallimulla' => 'Pallimulla',

'Gandara' => 'Gandara',

'Kamburugamuwa' => 'Kamburugamuwa',

'Polhena' => 'Polhena',

'Madiha' => 'Madiha',

    );
 
$address_fields['address_2']['label'] = ' Nearest Location';
$address_fields['address_2']['type'] = 'select';
$address_fields['address_2']['options'] = $location_array;
 
return $address_fields;
 
}



// Part 1: assign fee
// global $woocommerce;
// add_action( 'woocommerce_cart_calculate_fees', 'bbloomer_add_checkout_fee_for_gateway' );
  
// function bbloomer_add_checkout_fee_for_gateway() {
//     $chosen_gateway = WC()->session->get( 'chosen_payment_method' );
// 	if ( $chosen_gateway == 'payhere' ) {
//         $cart_total = WC()->cart->subtotal;
// 	$tax = $cart_total * 0.0299;
//       WC()->cart->add_fee( 'payhere Fee (2.99%)',$tax );
//    }
// }
  
// Part 2: reload checkout on payment gateway change
  
add_action( 'woocommerce_review_order_before_payment', 'bbloomer_refresh_checkout_on_payment_methods_change' );
  
function bbloomer_refresh_checkout_on_payment_methods_change(){
    ?>
<script type="text/javascript">
(function($) {
    $('form.checkout').on('change', 'input[name^="payment_method"]', function() {
        $('body').trigger('update_checkout');
    });
})(jQuery);
</script>
<?php
}

add_filter('woocommerce_billing_fields','wpb_custom_billing_fields');
function wpb_custom_billing_fields( $fields = array() ) {

	unset($fields['billing_company']);

	return $fields;
}

add_filter( 'woocommerce_sale_flash', 'add_percentage_to_sale_badge', 20, 3 );
function add_percentage_to_sale_badge( $html, $post, $product ) {
    if( $product->is_type('variable')){
        $percentages = array();

        // Get all variation prices
        $prices = $product->get_variation_prices();

        // Loop through variation prices
        foreach( $prices['price'] as $key => $price ){
            // Only on sale variations
            if( $prices['regular_price'][$key] !== $price ){
                // Calculate and set in the array the percentage for each variation on sale
                $percentages[] = round(( $prices['regular_price'][$key] - $prices['sale_price'][$key]));
            }
        }
        $percentage = max($percentages);
    } else {
        $regular_price = (float) $product->get_regular_price();
        $sale_price    = (float) $product->get_sale_price();

        $percentage    = round(($regular_price - $sale_price ));
    }
    return '<span class="onsale">' . 'රු ' . $percentage . esc_html__( ' ක් අඩුවෙන් ',  ) . '</span>';
}





// function bbloomer_wc_discount_total_30() {
 
//     global $woocommerce;
      
//     $discount_total = 0;
      
//     foreach ( $woocommerce->cart->get_cart() as $cart_item_key => $values) {
          
//    $_product = $values['data'];
  
//         if ( $_product->is_on_sale() ) {
//         $regular_price = $_product->get_regular_price();
//         $sale_price = $_product->get_sale_price();
//         $discount = ($regular_price - $sale_price) * $values['quantity'];
//         $discount_total += $discount;
//         }
  
//     }
            
//     if ( $discount_total > 0 ) {
//     echo '<tr class="cart-discount">
//     <th>'. __( 'You Saved', 'woocommerce' ) .'</th>
//     <td data-title=" '. __( 'You Saved', 'woocommerce' ) .' ">'
//     . wc_price( $discount_total + $woocommerce->cart->discount_cart ) .'</td>
//     </tr>';
//     }
 
// }
 
// // Hook our values to the Basket and Checkout pages
 
// add_action( 'woocommerce_cart_totals_after_order_total', 'bbloomer_wc_discount_total_30', 99);
// add_action( 'woocommerce_review_order_after_order_total', 'bbloomer_wc_discount_total_30', 99);

add_filter('add_to_cart_custom_fragments', 'woocommerce_header_add_to_cart_custom_fragment');
function woocommerce_header_add_to_cart_custom_fragment( $cart_fragments ) {
                global $woocommerce;
                ob_start();
                ?>
<a class="cart-contents" href="<?php echo $woocommerce->cart->get_cart_url(); ?>"
    title="<?php _e('View   cart', 'woothemes'); ?>"><?php echo sprintf(_n('%d item', '%d items', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count);?>
    - <?php echo $woocommerce->cart->get_cart_total(); ?></a>
<?php
                $cart_fragments['a.cart-contents'] = ob_get_clean();
                return $cart_fragments;
}function my_hide_shipping_when_free_is_available( $rates ) {
	$free = array();

	foreach ( $rates as $rate_id => $rate ) {
		if ( 'free_shipping' === $rate->method_id ) {
			$free[ $rate_id ] = $rate;
			break;
		}
	}

	return ! empty( $free ) ? $free : $rates;
}

add_filter( 'woocommerce_package_rates', 'my_hide_shipping_when_free_is_available', 100 );
add_filter( 'woocommerce_available_payment_gateways', 'bbloomer_unset_gateway_by_category' );
  
function bbloomer_unset_gateway_by_category( $available_gateways ) {
    if ( is_admin() ) return $available_gateways;
    if ( ! is_checkout() ) return $available_gateways;
    $unset = false;
    $category_id = array( 824 );
    foreach ( WC()->cart->get_cart_contents() as $key => $values ) {
        $terms = get_the_terms( $values['product_id'], 'product_cat' );    
        foreach ( $terms as $term ) {        
            if ( in_array( $term->term_id, $category_id ) ) {
                $unset = true;
                break;
            }
        }
    }
    if ( $unset == true ) unset( $available_gateways['cod'] );
    return $available_gateways;
}