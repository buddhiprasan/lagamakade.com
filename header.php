<?php
/**
 * The header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package lagamakade
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

$container = get_theme_mod( 'lagamakade_container_type' );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <?php wp_head(); ?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-161614294-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-161614294-1');
    </script>

</head>

<body <?php body_class(); ?> <?php lagamakade_body_attributes(); ?>>
    <!-- GetButton.io widget -->
    <!-- GetButton.io widget -->
    <script type="text/javascript">
    (function() {
        var options = {
            facebook: "110507073921302", // Facebook page ID
            whatsapp: "+94761100639", // WhatsApp number
            call_to_action: "Message us", // Call to action
            button_color: "#FF6550", // Color of button
            position: "right", // Position may be 'right' or 'left'
            order: "whatsapp,facebook", // Order of buttons
        };
        var proto = document.location.protocol,
            host = "getbutton.io",
            url = proto + "//static." + host;
        var s = document.createElement('script');
        s.type = 'text/javascript';
        s.async = true;
        s.src = url + '/widget-send-button/js/init.js';
        s.onload = function() {
            WhWidgetSendButton.init(host, proto, options);
        };
        var x = document.getElementsByTagName('script')[0];
        x.parentNode.insertBefore(s, x);
    })();
    </script>
    <!-- /GetButton.io widget -->
    <!-- /GetButton.io widget -->
    <?php do_action( 'wp_body_open' ); ?>

    <div class="site" id="page">

        <!-- ******************* The Navbar Area ******************* -->
        <div id="wrapper-navbar">

            <a class="skip-link sr-only sr-only-focusable"
                href="#content"><?php esc_html_e( 'Skip to content', 'lagamakade' ); ?></a>

            <nav class="navbar navbar-expand-lg navbar-light " aria-labelledby="main-nav-label">

                <h2 id="main-nav-label" class="sr-only">
                    <?php esc_html_e( 'Main Navigation', 'lagamakade' ); ?>
                </h2>
                <div class="container-fluid">
                    <!-- Your site title as branding in the menu -->
                    <?php if ( ! has_custom_logo() ) { ?>

                    <?php if ( is_front_page() && is_home() ) : ?>

                    <h1 class="navbar-brand mb-0"><a rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>"
                            title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"
                            itemprop="url"><?php bloginfo( 'name' ); ?></a></h1>

                    <?php else : ?>

                    <a class="navbar-brand" rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>"
                        title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"
                        itemprop="url"><?php bloginfo( 'name' ); ?></a>

                    <?php endif; ?>

                    <?php } else {
						the_custom_logo();
					} ?>
                    <!-- end custom logo -->



                    <div class="nav_menu">

                        <!-- The WordPress Menu goes here -->
                        <?php wp_nav_menu(
										array(
											'theme_location'  => 'primary',
											'container_class' => 'collapse navbar-collapse  justify-content-center',
											'container_id'    => 'navbarSupportedContent',
											'menu_class'      => 'navbar-nav cl-effect-3',
											'fallback_cb'     => '',
											'menu_id'         => 'main-menu',
											'depth'           => 2,
											'walker'          => new lagamakade_WP_Bootstrap_Navwalker(),
										)
                                    ); ?>


                    </div>


                    <ul class="list-inline mt-3">
                        <li class="list-inline-item nav_cart">

                            <!-- <a class="" href="<?php echo get_site_url(); ?>/cart/"><i
                                    class="fas fa-shopping-cart">
                                </i> View Cart</a> -->

                            <?php global $woocommerce; ?>
                            <a class="" href="<?php echo get_site_url(); ?>/cart/"
                                title="<?php _e('Cart View', 'woothemes'); ?>">
                                <i class="fas fa-shopping-cart"></i>
                                <?php echo sprintf(_n('%d item', '%d items', $woocommerce->cart->cart_contents_count, 'woothemes'),
                    $woocommerce->cart->cart_contents_count);?> -
                                <?php echo $woocommerce->cart->get_cart_total(); ?>
                            </a>
                            <!--Custom cart end-->
                        </li>

                        <li class="list-inline-item">
                            <div class="woo_product_search">

                                <?php get_product_search_form(); ?>
                            </div>
                        </li>
                    </ul>




                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                </div><!-- .container -->
            </nav><!-- .site-navigation -->
        </div><!-- #wrapper-navbar end -->
        <section id="index_offer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <h1>
                            Covid 19 වලින් ආරක්ශාවිමට නිවසේම රැදී සිටින්න, ඔබේ අවශතාවයන් ලඟම තැනින් ඔබේ නිවසටම නිවසටම
                            ඇනවුම් කරන්න.
                        </h1>
                    </div>
                </div>
            </div>
        </section>