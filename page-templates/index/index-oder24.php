<section id="oder24">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <ul class="list-inline">
                    <li class="list-inline-item"><a target="_blank" href="https://www.facebook.com/lagamakade/"><i
                                class="fab fa-facebook-square"></i> </a></li>
                    <li class="list-inline-item"> <a target="_blank" href="https://wa.me/07611006339"><i
                                class="fab fa-whatsapp-square"></i></a></li>
                    <li class="list-inline-item"><a target="_blank" href="m.me/lagamakade"><i
                                class="fab fa-facebook-messenger"></i></a></li>
                </ul>
            </div>
            <div class="col-md-12">
                <h2 class="text-center">
                    ලඟම තැනින් ඔබේ ඇනවුම ඔබේ නිවසටම
                </h2>
                <p>මෙම ක්‍රියාවලියේදී කිසියම් අපහසුතවයක් හෝ අඩුපාඩුවක් වන්නෙනම් අපගේ බලවත් කණගාටුව ප්‍රකශ කර සිටින අතර
                    එය අපවෙත දන්වන්න</p>
                <hr>
                <h3>වැඩිවිස්තර විමසීම්<a href="tel:07611006339"> 076 11 00 639</a>
                </h3>

            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="mt-5">
                    <h5>අන්තර්ජාල ගෙවීම්</h5>
                    <img src="https://www.payhere.lk/downloads/images/payhere_long_banner.png" alt="PayHere"
                        width="100%" />
                    <!-- <p class="mt-2"><small>*සෑම ඇනවුමක් සඳහාම සේවා ගාස්තුවක් අයකෙරේ | අන්තර්ජාල ගෙවීම් සඳහා 2.9% බදු
                            අමතරව අයකෙරේ</small></p> -->
                    <!-- <p class="free_lb">රු 7000 වැඩ් බිල්පත් සඳහා සේවා ගාස්තු අය නොකෙරේ</p> -->

                </div>
            </div>
        </div>

</section>