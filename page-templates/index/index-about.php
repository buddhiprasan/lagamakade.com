<section id="index_about">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <h2>Lagamakade.com ONLINE SHOPPING - WHO WE ARE</h2>
                <p>The Lagamakade website was created in the year 2020 to limit the public gathering to control the
                    coronavirus status. It also aims to empower consumers around the Matara District of Sri Lanka with
                    easy access to anything they want. Our market access is always based on trust and excellent customer
                    service; So you can get everything you need with the card payment as well as the cash on delivery
                    payment method. Also, all the items you have ordered will be delivered to your doorstep within 48
                    hours.</p>
                <p>We give our customers the ability to order from the largest selection of products at an attractive
                    price. This platform gives our customers the opportunity to easily get what they want without any
                    hassle. It also enables you to order and send items to your loved ones around Matara. Consumers can
                    choose from a variety of products through frozen, chilled, and ambient temperatures. Such products
                    include fresh vegetables, fruits, dairy products, fish, meat, poultry, and pharmacy products. You
                    can also easily find baby care products, laundry items, grocery items, cakes, foods, and gift items.
                </p>
                <p>We believe customer service is important and we are always happy to assist you over the phone, email,
                    or social media. Clicking on a few pages will take you a few steps away from getting any product you
                    like right to your home. With a growing product range, you have the ability to choose the product
                    you are looking for. Visit Sri Lanka's newest online grocery store for all your needs - from frozen
                    to fresh and everything in between! Now, you can order all your daily necessities from the comfort
                    of your own home or anywhere you want!</p>

            </div>
        </div>
    </div>
    </div>