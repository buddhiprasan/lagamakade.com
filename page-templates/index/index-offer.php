<section id="slider_section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3 col_padding">
                <div class="index_cat_btn">
                    <a href="https://lagamakade.com/store-listing/groceries/" class="btn d-flex "
                        type="button">Groceries<i class="fas fa-chevron-right"></i></a>
                    <a href="https://lagamakade.com/store-listing/?store_categories[]=food-restaurant"
                        class="btn d-flex" type="button">Food
                        Restaurant<i class="fas fa-chevron-right"></i></a>
                    <a href="https://lagamakade.com/store-listing/?store_categories[]=bakeries" class="btn d-flex"
                        type="button">Bakeries<i class="fas fa-chevron-right"></i></a>
                    <a href="https://lagamakade.com/store-listing/?store_categories[]=groceries" class="btn d-flex"
                        type="button">Fruit /
                        Vegetable<i class="fas fa-chevron-right"></i></a>
                    <a href="https://lagamakade.com/store-listing/?store_categories[]=outher-shop" class="btn d-flex"
                        type="button">Other
                        Shop<i class="fas fa-chevron-right"></i> </a>
                    <a href="https://lagamakade.com/store-listing/?store_categories[]=made-in-srilanka"
                        class="btn d-flex" type="button">Made
                        In Sri Lanka<i class="fas fa-chevron-right"></i> </a>
                </div>
            </div>
            <div class="col-lg-6 col_padding">
                <div id="carouselExampleFade" class="carousel slide carousel-fade mb-4" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active" data-interval="6000">
                            <a href="https://lagamakade.com/create-shop/"><img
                                    src="<?php bloginfo('template_url'); ?>/img/new  shop 1.jpg" class="d-block w-100"
                                    alt="lagamakade.com">
                        </div>
                        <div class="carousel-item" data-interval="6000">
                            <a href="https://lagamakade.com/store-listing/"><img
                                    src="<?php bloginfo('template_url'); ?>/img/new shop 2.jpg" class="d-block w-100"
                                    alt="lagamakade.com">
                        </div>
                        <div class="carousel-item " data-interval="6000">
                            <a href="https://lagamakade.com/store-listing/"><img
                                    src="<?php bloginfo('template_url'); ?>/img/banner new 4.jpg" class="d-block w-100"
                                    alt="agamakade.com"></a>
                        </div>

                        <div class="carousel-item" data-interval="6000">
                            <a tager target="_blank"
                                href="https://play.google.com/store/apps/details?id=com.hexacode.lagama_kade&hl=en_CA&gl=US"><img
                                    src="<?php bloginfo('template_url'); ?>/img/mobile app.jpg" class="d-block w-100"
                                    alt="lagamakade.com">
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col_padding">
                <div class="index_banner_img">
                    <a href="https://lagamakade.com/create-shop/"> <img
                            src="<?php bloginfo('template_url'); ?>/img/new shop 1.jpg" class="d-block w-100"
                            alt="agamakade.com"></a>

                </div>
                <div class="index_banner_img">
                    <a href="https://lagamakade.com/store-listing/"> <img
                            src="<?php bloginfo('template_url'); ?>/img/new shop r 2.jpg" class="d-block w-100"
                            alt="agamakade.com"></a>

                </div>
                <div class="index_banner_img">
                    <a href="https://lagamakade.com/about-us/"> <img
                            src="<?php bloginfo('template_url'); ?>/img/new shop r 3.jpg" class="d-block w-100"
                            alt="agamakade.com"></a>

                </div>

            </div>
        </div>
    </div>
</section>
<section id="index_cat_slider">
    <div class="container-fluid">
        <div class=" row">
            <div class="col-md-12"> </div>


        </div>
    </div>

</section>