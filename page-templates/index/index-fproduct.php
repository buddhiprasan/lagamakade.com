<section id="index_fe_product">
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <h3>Aurudhu Package</h3>
                <?php echo do_shortcode('[featured_products limit="5" columns="5" category="aurudu" orderby="popularity" order=”ASC” ]' ); ?>

            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <h3>On Sale</h3>
                <?php echo do_shortcode('[products limit="15" columns="5" orderby="popularity" class="quick-sale" on_sale="true" ] ' ); ?>

            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <h3>Groceries</h3>
                <?php echo do_shortcode('[featured_products limit="20" columns="5" category="grocerys" orderby="popularity" order=”ASC” ]' ); ?>

            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <h3>Health Care</h3>
                <?php echo do_shortcode('[featured_products limit="5" columns="5" category="pharmacy" orderby="popularity" order=”ASC” ]' ); ?>

            </div>
        </div>


        <div class="row">
            <div class="col-md-12">
                <h3>Personal Care</h3>
                <?php echo do_shortcode('[featured_products limit="5" columns="5" category="personal-care" orderby="popularity" order=”ASC” ]' ); ?>

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h3>Vegetable</h3>
                <?php echo do_shortcode('[featured_products limit="10" columns="5" category="vegetable" orderby="popularity" order=”ASC”]' ); ?>

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h3>Made In Matara</h3>
                <?php echo do_shortcode('[featured_products limit="10" columns="5" category="made-in-matara" ]' ); ?>
            </div>
        </div>
</section>