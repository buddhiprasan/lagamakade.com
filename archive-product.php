<?php
/**
 * The template for displaying archive pages
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package lagamakade
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
?>
<section id="archive_page">

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <?php woocommerce_breadcrumb(); ?>
                <header class="woocommerce-products-header">
                    <?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
                    <h1 class="woocommerce-products-header__title page-title"><?php woocommerce_page_title(); ?></h1>
                    <?php endif; ?>
                    <?php do_action( 'woocommerce_archive_description' );?>
                </header>
            </div>
        </div>
        <div class="row">

            <div class="col-lg-3 col-sm-12 order-lg-1 order-2">
                <div class="left_sidebar">
                    <div class="left_sidebar_text">
                        <!-- <h2>FILTERS</h2> -->
                    </div>
                    <?php get_template_part( 'sidebar-templates/sidebar', 'left' ); ?>
                </div>
            </div>
            <div class="col-lg-9 col-sm-12 order-lg-2 order-1">
                <div class="archive_page">
                    <?php
                
            
                


                function squirrel_product_subcategories( $args = array() ) {
                    $parentid = get_queried_object_id();
                    
                    $counter = 1; //counter tracks the number of the post we're on 
                         
                    $args = array(
                        'parent' => $parentid
                    );
                 
                    $terms = get_terms( 'product_cat', $args );
                 
                    if ( $terms ) {
                         
                        echo '<ul class="products columns-5">';
                     
                            foreach ( $terms as $term ) {
                                
                                // add last or first classes          
                                ?>
                    <?php if ($counter % 10 == 0) { echo '<li class="product-category  squirrel-cat last">';} 
                                  elseif ($counter % 10 == 1) { echo '<li class="product-category squirrel-cat first">';} 
                                  else                       { echo '<li class="product-category  squirrel-cat">';} 
                                  ?>
                    <?php
                                         
                                // echo '<li class="product-category product squirrel-cat ">';                 
                                     
                                    echo '<a href="' .  esc_url( get_term_link( $term ) ) . '" class="' . $term->slug . '" squirrel-cat-a>';
                                    
                                    woocommerce_subcategory_thumbnail( $term );
                                    
                                    echo '<h2 class="woocommerce-loop-category__title squirrel-cat-h2">';
                                    echo $term->name;
                                    echo '</h2>';
                                    echo '</a>';
                                                                                     
                            echo '</li>';
                            
                            $counter++; // This increases the value of $counter by 1 for every loop iteration 
                                                                                     
                        }
                     
                        echo '</ul>';
                        echo '<hr>';
                
                 
                    }
                }
                
                add_action( 'woocommerce_before_shop_loop', 'squirrel_product_subcategories', 50 );
                
              
                
                
                ?>

                    <?php
            if ( woocommerce_product_loop() ) {
	            do_action( 'woocommerce_before_shop_loop' );
                    woocommerce_product_loop_start();
                    if ( wc_get_loop_prop( 'total' ) ) {
                        while ( have_posts() ) {
                            the_post();
                            do_action( 'woocommerce_shop_loop' );
                            wc_get_template_part( 'content', 'product' );
                        }
                    }
                        woocommerce_product_loop_end();
                        do_action( 'woocommerce_after_shop_loop' );
                    } else {

                        do_action( 'woocommerce_no_products_found' );
                    }
                    ?>

                </div>
            </div>
        </div>
    </div>

</section>
<?php get_footer();