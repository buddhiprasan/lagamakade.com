<footer id="footer">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="gov_section">
                    <img class="mx-auto d-block"
                        src="<?php echo get_template_directory_uri(); ?>/img/Emblem_of_Sri_Lanka.svg.png" alt="">
                    <h2>අදීක්ෂණය ප්‍රාදේශීය ලේකම් කාර්යාලය - මාතර</h2>
                    <hr>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-4 col-md-12">
                <h3>තාක්ෂණික විමසීම්<a href="tel:0712517775"> 071 251 777 5</a>
                </h3>
            </div>
            <div class="col-lg-4 col-md-12">
                <h3>ප්‍රාදේශීය ලේකම් - මාතර<a href="tel:0718613770"> 071 861 377 0</a>
                </h3>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <ul class="list-inline justify-content-center">
                    <li class="list-inline-item border-right pr-2 border-secondary"><a
                            href="https://www.lagamakade.com/privacy-policy/">Privacy
                            Policy</a> </li>
                    <li class="list-inline-item"><a href="https://www.lagamakade.com/terms-and-conditions/">Terms &
                            Condition</a></li>
                </ul>
                <hr>
                <p>කොරොනා වෛයිරස තවය පාලනය කිරිම උදෙසා මහජන තාව එකතුවීම සීමා කිරිම වෙනුවෙන් සකසන ලද්දකි</p>
                <p class="pt-0"><?php echo date('Y'); ?> By <a href="https://www.facebook.com/buddhiprasan">Buddhi
                        Prasan</a></p>
            </div>
        </div>
    </div>
</footer>
<?php wp_footer(); ?>
</body>

</html>